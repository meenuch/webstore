﻿using System;
using System.Collections.Generic;

namespace webStore.Models
{
    public partial class PageSetting
    {
        public string PageName { get; set; }
        public bool Home { get; set; }
        public bool Support { get; set; }
        public bool AboutMst { get; set; }
        public bool Login { get; set; }
        public bool AdminMenu { get; set; }
    }
}
