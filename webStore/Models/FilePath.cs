﻿using System;
using System.Collections.Generic;

namespace webStore.Models
{
    public partial class FilePath
    {
        public int Id { get; set; }
        public string TypeFile { get; set; }
        public string OriginalFileName { get; set; }
        public string FileExtension { get; set; }
        public string ContentType { get; set; }
        public int? ContentLength { get; set; }
        public string ServerFilePath { get; set; }
        public string WebPath { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
    }
}
