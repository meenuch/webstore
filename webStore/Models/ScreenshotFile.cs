﻿using System;
using System.Collections.Generic;

namespace webStore.Models
{
    public partial class ScreenshotFile
    {
        public int AppId { get; set; }
        public int FileId { get; set; }
    }
}
