﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace webStore.Models
{
    public partial class UserMaster
    {
        [Required(ErrorMessage = "Please Enter Username !!")]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public string UserLevel { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }

        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public bool IsActive { get; set; }
    }
}