﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace webStore.Models
{
    public partial class MST_AppStoreContext : DbContext
    {
        public MST_AppStoreContext()
        {
        }

        public MST_AppStoreContext(DbContextOptions<MST_AppStoreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AppStore> AppStore { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<FilePath> FilePath { get; set; }
        public virtual DbSet<PageSetting> PageSetting { get; set; }
        public virtual DbSet<ScreenshotFile> ScreenshotFile { get; set; }
        public virtual DbSet<UserMaster> UserMaster { get; set; }
        public virtual DbSet<VAppStore> VAppStore { get; set; }
        public virtual DbSet<VScreenshot> VScreenshot { get; set; }
        public virtual DbSet<VersionHistory> VersionHistory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost\\MST;Database=MST_AppStore;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppStore>(entity =>
            {
                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AndroidAppFileUrl)
                    .HasColumnName("AndroidAppFileURL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AndroidAppSize)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Company)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Detail).IsUnicode(false);

                entity.Property(e => e.IconAppId).HasColumnName("IconAppID");

                entity.Property(e => e.InformationCompatibility).IsUnicode(false);

                entity.Property(e => e.InformationCopyright)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InformationLanguages)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InformationProvider)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IosappFileSize)
                    .HasColumnName("IOSAppFileSize")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IosappFileUrl)
                    .HasColumnName("IOSAppFileURL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveIos).HasColumnName("IsActiveIOS");

                entity.Property(e => e.LastVersionDate).HasColumnType("date");

                entity.Property(e => e.LastVersionDetail).IsUnicode(false);

                entity.Property(e => e.LastVersionNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PublishIos).HasColumnName("PublishIOS");

                entity.Property(e => e.Title)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifyBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FilePath>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ContentType).IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileExtension)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.OriginalFileName).IsUnicode(false);

                entity.Property(e => e.ServerFilePath).IsUnicode(false);

                entity.Property(e => e.TypeFile)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.WebPath).IsUnicode(false);
            });

            modelBuilder.Entity<PageSetting>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.PageName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ScreenshotFile>(entity =>
            {
                entity.HasKey(e => new { e.AppId, e.FileId })
                    .HasName("PK_ScreenshowFile");

                entity.Property(e => e.AppId).HasColumnName("AppID");
            });

            modelBuilder.Entity<UserMaster>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.HasIndex(e => e.Email)
                    .HasName("IX_UserMaster")
                    .IsUnique();

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserLevel)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VAppStore>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_AppStore");

                entity.Property(e => e.AndroidAppFileUrl)
                    .HasColumnName("AndroidAppFileURL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AndroidAppSize)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Detail).IsUnicode(false);

                entity.Property(e => e.IconAppId).HasColumnName("IconAppID");

                entity.Property(e => e.IconContentType).IsUnicode(false);

                entity.Property(e => e.IconFileExtension)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IconOriginalFileName).IsUnicode(false);

                entity.Property(e => e.IconServerFilePath).IsUnicode(false);

                entity.Property(e => e.IconWebPath).IsUnicode(false);

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.InformationCompatibility).IsUnicode(false);

                entity.Property(e => e.InformationCopyright)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InformationLanguages)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InformationProvider)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IosappFileSize)
                    .HasColumnName("IOSAppFileSize")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IosappFileUrl)
                    .HasColumnName("IOSAppFileURL")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsActiveIos).HasColumnName("IsActiveIOS");

                entity.Property(e => e.LastVersionDate).HasColumnType("date");

                entity.Property(e => e.LastVersionDetail).IsUnicode(false);

                entity.Property(e => e.LastVersionNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PublishIos).HasColumnName("PublishIOS");

                entity.Property(e => e.Title)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VScreenshot>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("v_Screenshot");

                entity.Property(e => e.AppId).HasColumnName("AppID");

                entity.Property(e => e.ContentType).IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.FileExtension)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifyDate).HasColumnType("datetime");

                entity.Property(e => e.OriginalFileName).IsUnicode(false);

                entity.Property(e => e.ServerFilePath).IsUnicode(false);

                entity.Property(e => e.TypeFile)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WebPath).IsUnicode(false);
            });

            modelBuilder.Entity<VersionHistory>(entity =>
            {
                entity.HasKey(e => new { e.AppId, e.VersionNumber });

                entity.Property(e => e.AppId).HasColumnName("AppID");

                entity.Property(e => e.VersionNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VersionDate).HasColumnType("date");

                entity.Property(e => e.VersionDetail).IsUnicode(false);
            });

            modelBuilder.HasSequence("s_Appstore").StartsAt(2);

            modelBuilder.HasSequence("s_FilePath").StartsAt(84);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
