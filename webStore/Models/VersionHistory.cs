﻿using System;
using System.Collections.Generic;

namespace webStore.Models
{
    public partial class VersionHistory
    {
        public int AppId { get; set; }
        public string VersionNumber { get; set; }
        public string VersionDetail { get; set; }
        public DateTime? VersionDate { get; set; }
    }
}
