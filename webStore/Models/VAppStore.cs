﻿using System;
using System.Collections.Generic;

namespace webStore.Models
{
    public partial class VAppStore
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int IconAppId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Company { get; set; }
        public string Detail { get; set; }
        public string LastVersionNumber { get; set; }
        public DateTime? LastVersionDate { get; set; }
        public string LastVersionDetail { get; set; }
        public string InformationProvider { get; set; }
        public string InformationCompatibility { get; set; }
        public string InformationLanguages { get; set; }
        public string InformationCopyright { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string ModifyBy { get; set; }
        public int Seq { get; set; }
        public string IconOriginalFileName { get; set; }
        public string IconFileExtension { get; set; }
        public string IconContentType { get; set; }
        public int? IconContentLength { get; set; }
        public string IconServerFilePath { get; set; }
        public string IconWebPath { get; set; }
        public string IosappFileUrl { get; set; }
        public string IosappFileSize { get; set; }
        public string AndroidAppFileUrl { get; set; }
        public string AndroidAppSize { get; set; }
        public bool IsActiveAndroid { get; set; }
        public bool IsActiveIos { get; set; }
        public bool PublishAndroid { get; set; }
        public bool PublishIos { get; set; }
    }
}
