using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using webStore.Models;

namespace webStore.Pages.User
{
    public class IndexModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        public IndexModel(MST_AppStoreContext db)
        {
            _db = db;
        }

        public IEnumerable<UserMaster> allUser;

        [BindProperty]
        public UserMaster userSelect { get; set; }

        public async Task OnGet()
        {
            string username = HttpContext.Session.GetString("Username");
            allUser = await _db.UserMaster.Where(x => x.Username != username).ToListAsync();
        }

        public async Task<IActionResult> OnPost()
        {
            var userEdit = await _db.UserMaster.FirstAsync(x => x.Username == userSelect.Username);
            if (userEdit != null)
            {
                userEdit.Username = userSelect.Username;
                userEdit.FirstName = userSelect.FirstName;
                userEdit.LastName = userSelect.LastName;
                userEdit.Email = userSelect.Email;
                userEdit.ModifyDate = DateTime.Now;
                userEdit.ModifyBy = HttpContext.Session.GetString("Username");
                await _db.SaveChangesAsync();
                return RedirectToPage("/User/Index");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostDelete(string username)
        {
            var userDelete = await _db.UserMaster.FindAsync(username);
            if (userDelete == null)
            {
                return NotFound();
            }
            _db.UserMaster.Remove(userDelete);
            await _db.SaveChangesAsync();

            return RedirectToPage("/User/Index");
        }

        public async Task<IActionResult> OnPostExportExcel()
        {
            var list = await _db.UserMaster.ToListAsync();
            var stream = new MemoryStream();

            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            string excelName = $"UserList-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";
            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(list, true);
                package.Save();
                HttpContext.Response.Clear();
                HttpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Response.Headers.Add("Content-Disposition", string.Format("attachment;filename={0}", excelName));
                HttpContext.Response.Body.WriteAsync(package.GetAsByteArray());
            }
            stream.Position = 0;
            //return File(stream, "application/octet-stream", excelName);
            return RedirectToPage("/User/Index");
        }
    }
}