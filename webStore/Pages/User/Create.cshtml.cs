using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using webStore.Models;

namespace webStore.Pages.User
{
    public class CreateModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        public CreateModel(MST_AppStoreContext db)
        {
            _db = db;
        }

        [BindProperty]
        public UserMaster user { get; set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                user.UserLevel = "user";
                user.CreateDate = DateTime.Now;
                user.IsActive = true;
                user.CreateBy = HttpContext.Session.GetString("Username");
                await _db.UserMaster.AddAsync(user);
                await _db.SaveChangesAsync();
                return RedirectToPage("/User/Index");
            }
            else
            {
                return Page();
            }
        }
    }
}