using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using webStore.Models;

namespace webStore.Pages.App
{
    public class ViewAllModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        public ViewAllModel(MST_AppStoreContext db)
        {
            _db = db;
        }

        public Category category;

        public IEnumerable<VAppStore> vAppstore;

        public async Task OnGet(int id)
        {
            category = await _db.Category.FindAsync(id);
            if (category != null)
            {
                vAppstore = _db.VAppStore.Where(x => x.CategoryId == category.Id).ToList();
            }
        }
    }
}