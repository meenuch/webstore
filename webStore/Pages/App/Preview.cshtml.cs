using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using webStore.Models;

namespace webStore.Pages.App
{
    public class PreviewModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        public PreviewModel(MST_AppStoreContext db)
        {
            _db = db;
        }

        public VAppStore Appstore;

        public IEnumerable<VScreenshot> vScreenshots;

        public IEnumerable<VersionHistory> versionHistories;

        public async Task OnGet(int id)
        {
            Appstore = await _db.VAppStore.SingleAsync(x => x.Id == id);
            vScreenshots = await _db.VScreenshot.Where(x => x.AppId == id).ToListAsync();
            versionHistories = await _db.VersionHistory.Where(x => x.AppId == id).ToListAsync();
        }
    }
}