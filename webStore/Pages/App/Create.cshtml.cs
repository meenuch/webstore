using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using webStore.Models;

namespace webStore.Pages.App
{
    public class CreateModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        private IHostingEnvironment _env;

        public CreateModel(MST_AppStoreContext db, IHostingEnvironment env)
        {
            _db = db;
            _env = env;
        }

        [BindProperty]
        public AppStore appstore { get; set; }

        [BindProperty]
        public IEnumerable<Category> Categories { get; set; }

        [BindProperty]
        public IFormFile Upload { get; set; }

        [BindProperty]
        public IEnumerable<IFormFile> UploadIpadFile { get; set; }

        [BindProperty]
        public IEnumerable<IFormFile> UploadMobileFile { get; set; }

        public SelectList CategorySelectList { get; set; }

        [BindProperty]
        public int SelectedCetegoryId { get; set; }

        public async Task OnGet()
        {
            var categoriesSelect = await _db.Category.Where(x => x.IsActive == true).ToListAsync();
            CategorySelectList = new SelectList(categoriesSelect, nameof(Category.Id), nameof(Category.Name));
        }

        public async Task<IActionResult> OnPost()
        {
            var getIntAppId = _db.AppStore.ToList().Last();
            var getIntFilePathId = _db.FilePath.ToList().Last();

            int AppId = getIntAppId.Id + 1;
            int PathId = getIntFilePathId.Id;

            AppStore Application = new AppStore();
            Application.Id = AppId;
            Application.Name = appstore.Name;
            Application.Title = appstore.Title;
            Application.CategoryId = SelectedCetegoryId;
            Application.Company = "MST";
            Application.Detail = appstore.Detail;
            Application.InformationCompatibility = appstore.InformationCompatibility;
            Application.InformationCopyright = appstore.InformationCopyright;
            Application.InformationLanguages = appstore.InformationLanguages;
            Application.InformationProvider = appstore.InformationProvider;

            //upload icon app
            var webRoot = _env.WebRootPath;
            var value = Guid.NewGuid().ToString();
            var directoryPath = Path.Combine(webRoot, "application\\" + value + "\\");
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            var directoryIconPath = Path.Combine(directoryPath, "Icon\\");
            if (!Directory.Exists(directoryIconPath))
            {
                Directory.CreateDirectory(directoryIconPath);
            }
            var Fullpath = System.IO.Path.Combine(directoryIconPath, Upload.FileName);
            using (var fileStream = new FileStream(Fullpath, FileMode.Create))
            {
                await Upload.CopyToAsync(fileStream);
                FilePath fp = new FilePath()
                {
                    Id = PathId + 1,
                    TypeFile = "icon",
                    OriginalFileName = Upload.FileName,
                    WebPath = "/application/" + value + "/Icon/" + Upload.FileName,
                    CreateDate = DateTime.Now,
                    CreateBy = HttpContext.Session.GetString("Username")
                };
                await _db.FilePath.AddAsync(fp);
                await _db.SaveChangesAsync();
                PathId += 1;
                Application.IconAppId = fp.Id;
            }
            //Upload screenshot ipad
            var directoryIpadPath = Path.Combine(directoryPath, "Ipad\\");
            if (!Directory.Exists(directoryIpadPath))
            {
                Directory.CreateDirectory(directoryIpadPath);
            }
            foreach (var item in UploadIpadFile)
            {
                var mapPath = System.IO.Path.Combine(directoryIpadPath, item.FileName);
                using (var fileStream = new FileStream(mapPath, FileMode.Create))
                {
                    await item.CopyToAsync(fileStream);
                    FilePath fp = new FilePath()
                    {
                        Id = PathId + 1,
                        TypeFile = "ipad",
                        OriginalFileName = item.FileName,
                        WebPath = "/application/" + value + "/Ipad/" + item.FileName,
                        CreateDate = DateTime.Now,
                        CreateBy = HttpContext.Session.GetString("Username")
                    };
                    await _db.FilePath.AddAsync(fp);
                    await _db.SaveChangesAsync();
                    ScreenshotFile sf = new ScreenshotFile()
                    {
                        AppId = Application.Id,
                        FileId = fp.Id
                    };
                    await _db.ScreenshotFile.AddAsync(sf);
                    await _db.SaveChangesAsync();
                    PathId += 1;
                }
            }
            //Upload screenshot mobile
            var directoryMobilePath = Path.Combine(directoryPath, "Mobile\\");
            if (!Directory.Exists(directoryMobilePath))
            {
                Directory.CreateDirectory(directoryMobilePath);
            }
            foreach (var item in UploadMobileFile)
            {
                var mapPath = System.IO.Path.Combine(directoryMobilePath, item.FileName);
                using (var fileStream = new FileStream(mapPath, FileMode.Create))
                {
                    await item.CopyToAsync(fileStream);
                    FilePath fp = new FilePath()
                    {
                        Id = PathId + 1,
                        TypeFile = "mobile",
                        OriginalFileName = item.FileName,
                        WebPath = "/application/" + value + "/Mobile/" + item.FileName,
                        CreateDate = DateTime.Now,
                        CreateBy = HttpContext.Session.GetString("Username")
                    };
                    await _db.FilePath.AddAsync(fp);
                    await _db.SaveChangesAsync();
                    ScreenshotFile sf = new ScreenshotFile()
                    {
                        AppId = Application.Id,
                        FileId = fp.Id
                    };
                    await _db.ScreenshotFile.AddAsync(sf);
                    await _db.SaveChangesAsync();
                    PathId += 1;
                }
            }
            Application.CreateDate = DateTime.Now;
            Application.CreateBy = HttpContext.Session.GetString("Username");
            await _db.AppStore.AddAsync(Application);
            await _db.SaveChangesAsync();
            return RedirectToPage("/App/MyApp", new { username = HttpContext.Session.GetString("Username") });
        }
    }
}