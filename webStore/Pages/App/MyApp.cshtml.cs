using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using webStore.Models;

namespace webStore.Pages.App
{
    public class MyAppModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        public MyAppModel(MST_AppStoreContext db)
        {
            _db = db;
        }

        public IEnumerable<VAppStore> vAppstores;

        public async Task OnGet(string username)
        {
            vAppstores = await _db.VAppStore.Where(x => x.CreateBy == username).ToListAsync();
        }
    }
}