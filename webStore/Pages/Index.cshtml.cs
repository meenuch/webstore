﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using webStore.Models;

namespace webStore.Pages
{
    public class IndexModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        public IndexModel(MST_AppStoreContext db)
        {
            _db = db;
        }

        public IEnumerable<Category> categories;

        public IEnumerable<VAppStore> vAppstore;

        public async Task OnGet()
        {
            vAppstore = await _db.VAppStore.ToListAsync();
            categories = await _db.Category.ToListAsync();
        }
    }
}