using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using webStore.Models;

namespace webStore.Pages.Login
{
    public class IndexModel : PageModel
    {
        private readonly MST_AppStoreContext _db;

        public IndexModel(MST_AppStoreContext db)
        {
            _db = db;
        }

        [BindProperty]
        public UserMaster User { get; set; }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            var loginValidate = _db.UserMaster.SingleOrDefault(x => x.Username == User.Username && x.Password == User.Password);
            if (loginValidate != null)
            {
                HttpContext.Session.SetString("Username", User.Username);
                return RedirectToPage("/Index");
            }
            ViewData["alerterror"] = "Username or Password incorrect";
            return Page();
        }

        public IActionResult OnPostLogout()
        {
            HttpContext.Session.Clear();
            return RedirectToPage("/Login/Index");
        }
    }
}